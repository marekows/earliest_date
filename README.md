# Eealiest Date Generator  
  

## Purpose
Print earliest date generated from passed input  
  

## For installing it:  
1. Python 3 is required     
2. Change directory for with you want to keep the app  
3. Execute: `git clone https://marekows@bitbucket.org/marekows/earliest_date.git`


## Using:
In directory where app was cloned, execute:
```sh 
$ python earliest_date <input>
```   
`<input>`: It can be file name (path to it can be passed) or text.   
Passed text or text in the file should be in format: 3 numbers seperated with `/`,  eg. 3/1/6".
  
Samples:     
```sh
$ python earliest_date 4/10/2
$ 2002-04-10
$ cat filesample
$ 43/27/11
$ python earliest_date filesample
$ 2043-11-27
```

Numbers can contains 1, 2, 4 digits. 1,2 digits numbers can be zero padded.
Dates range: from 2000-01-01 to 2999-12-31.
Years can be given with 2 digits as well.
Eg. Year 2000 could be given as "2000", "00" or "0", year '2035' as '2035' or '35', year '2333' can be given only by '2333' - 3 digits numbers are not supported.  
