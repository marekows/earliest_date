import calendar
import sys


def collect_input():
    '''
    if sys.argv[1] is a name of exist file (full path can be passed):
        return content of the file
    else return sys.argv[1] value
    '''
    try:
        date_resource = sys.argv[1]
        date_input = open(date_resource,"r").read().rstrip()
    except IOError:
        date_input = date_resource
    except IndexError:
        sys.exit("You need to pass parameter.\nParameter can be numbers in proper format (eg. '1/2/2000') or filename with them")
        # raise
    return date_input

def prepare_input(date_input):
    '''
    Takes one parameter, type: string in format:
    '4/1/5' - 3 numbers separated with '/'
    At most one of the numbers has four digits, 
    and the others have one or two digits.
    
    Return: list of integers with value from
    each number
    '''
    error_msg = "%s is illegal" % (date_input)
    numbers = date_input.split('/')
    if not len(numbers) == 3: sys.exit(error_msg)

    len_numbers = []

    for index, number in enumerate(numbers):
        try:
            numbers[index] = int(numbers[index])
        except ValueError:
            sys.exit(error_msg)

        len_numbers.append(len(number))

    if 4 in len_numbers:
        if not len_numbers.count(1) + len_numbers.count(2) == 2: sys.exit(error_msg)
        if 0 in numbers: sys.exit(error_msg)
        if not 2000 <= max(numbers) <= 2999: sys.exit(error_msg)
    else:
        if not len_numbers.count(1) + len_numbers.count(
            2) == 3: sys.exit(error_msg)
        if not numbers.count(0) <= 1: sys.exit(error_msg)

    return numbers


def generate_all_posible_dates(numbers):
    '''
    Takes one parameter - list of 3 integers.
    Combine all 3 integers in all possible ways and
    try to generate date for each combination.
    Add each generated date to list called 'dates'
    Return: list with dates
    '''
    dates = []

    for number in numbers:
        for index in range(3):
            if index != numbers.index(number):
                try:
                    if number == 0:
                        year = 2000
                    if number < 2000:
                        year = number + 2000
                    else:
                        year = number
                    days = calendar.monthrange(year, numbers[index])[1]
                    day = list(numbers)
                    day.remove(number)
                    day.remove(numbers[index])
                    assert day[0] in range(days+1)
                    dates.append("%s-%02d-%02d" % (year, numbers[index], day[0]))
                except (AssertionError, calendar.IllegalMonthError):
                    pass
    return dates

def pick_lowest_date(dates):
    '''
    Takes one parameter - list of dates.
    Return earliest date in passed list
    If list is empty: return None
    '''
    if len(dates) >= 1:
        earliest_date = min(dates) 
    else:
        return None
    return earliest_date

def get_final_output(earliest_date, date_input):
    '''
    Takes two parameters. 
    Param1 for passing date to print
    Param2 for passing initial input
    if date is not None:
        return date 
    else date
        return: initial input value and failure information
    '''
    if earliest_date:
        return earliest_date
    else:
        return "%s is illegal" % date_input
    
    