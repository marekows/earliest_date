from common import collect_input, prepare_input, generate_all_posible_dates, pick_lowest_date, get_final_output

# Collecting input from passed file or string as sysarg[1]
date_input = collect_input()

# Processing of input for validation and finally collect list of integers
numbers = prepare_input(date_input)

# Generate each possible date from passed list
dates = generate_all_posible_dates(numbers)

# Pick lowest date from passed list with dates
earliest_date = pick_lowest_date(dates)


final_output = get_final_output(earliest_date, date_input)

print(final_output)